## Interface: 20501
## Title: Atlas or World Map
## Notes: Opens Atlas in dungeons and raids, opens the world map elsewhere
## Author: Wildmane-Venoxis
## Version: 0.1
## DefaultState: Enabled
## Dependencies: Atlas
AtlasOrWorldMap.lua
