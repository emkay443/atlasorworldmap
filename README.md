# Atlas or World Map
Simple AddOn for World of Warcraft Classic that opens Atlas in dungeons and raids, and the world map elsewhere.

## Dependencies
[Atlas (Classic)](https://www.wowinterface.com/downloads/info25605-AtlasClassic.html)

## Installation
Clone this repository and copy the AtlasOrWorldMap folder to your WoW Classic Addon directory (for example `C:\Program Files\World of Warcraft\_classic_\Interface\AddOns`)

## Usage
Press ESC, open "Key Bindings", go to "Other" and set a key binding for "Open Atlas or World Map" (for example 'M', the default hotkey for opening the world map).